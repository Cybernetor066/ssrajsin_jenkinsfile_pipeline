#!/usr/bin/bash
# script name: automate_health_check.sh
# Login to one account


declare -a my_arr=()

# Append the array from aws_access file
input="aws_access"
# input="${workspace}/app/aws_access"
while IFS= read -r line
do
    echo "$line"
    my_arr+=("$line")

done < "$input"


# echo "${my_arr[@]}"


for item in "${my_arr[@]}"; do
    IFS=':'
    read -a strarr <<< "$item"
    # echo "The line with $item will be split shortly"

    # Using it to set variables
    ACCESS_KEY_ID="${strarr[0]}"
    SECRET_ACCESS_KEY="${strarr[1]}"
    AWS_REGION="${strarr[2]}"
    AWS_OUTPUT_FORMAT="${strarr[3]}"

    echo $ACCESS_KEY_ID
    echo $SECRET_ACCESS_KEY
    echo $AWS_REGION
    echo $AWS_OUTPUT_FORMAT
    
    aws configure set aws_access_key_id "${ACCESS_KEY_ID}"
    aws configure set aws_secret_access_key "${SECRET_ACCESS_KEY}"
    aws configure set region "${AWS_REGION}"
    aws configure set output "${AWS_OUTPUT_FORMAT}"


    # Real request
    whoami
    # export ENV_VALUE="dev"
    echo "Checking environment variable!"
    echo "Env Value is ${ENV_VALUE}"
    # Run your aws cli operation here
    # e.g aws s3 ls
    python3 script01.py -e ${ENV_VALUE}
    # python3 ${workspace}/app/script01.py -e ${ENV_VALUE}

    # Reading file created from python script01
    python3 script02.py -e ${ENV_VALUE}
    # python3 ${workspace}/app/script02.py -e ${ENV_VALUE}

    echo "Operation Success!!"


done









