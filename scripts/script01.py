import sys, os, time, getopt

# Accessing files in directories
if getattr(sys, 'frozen', False):
    # running in a bundled form
    base_dir = sys._MEIPASS # pylint: disable=no-member
else:
    # running normally
    base_dir = os.path.dirname(os.path.abspath(__file__))


# # Locating helper files in the current working directory
# config_file_path = os.path.join(base_dir, 'config.ini')


# Set print direction
script01_logfile = os.path.join(base_dir, 'script01_log.txt')
# sys.stdout = open(script01_logfile, 'w')

# # sys.argv is the list of command-line arguments.
# # len(sys.argv) is the number of command-line arguments.
# # Here sys.argv[0] is the program ie. script name.


# print('Number of arguments:', len(sys.argv), 'arguments.')
# print('Argument List:', str(sys.argv))



def main(argv):
    env_value = ''
    try:
        # For the ""he:",["envValue="]" line, the : that follows e means it will be expecting a value as in -e <value>
        # While for the h, no colon (:) means it can be called without an argument.
        # The enclosed ["envValue="] is for long option thats specifying like this "--envValue=<value>"
        opts, args = getopt.getopt(argv,"he:",["envValue="])
    except getopt.GetoptError:
        print(f'Wrong format!, use: {sys.argv[0]} -e <environment_value>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(f'USAGE: {sys.argv[0]} -e <environment_value>')
            sys.exit()
        elif opt in ("-e", "--envValue"):
            env_value = arg


    print('Environment value selected is: ', env_value)
    # Calling endpoints based on env values passed
    if env_value == 'dev':
        # Call endpoint for "dev"
        print('Calling endpoint for dev')
        with open(script01_logfile, 'w') as f_open:
            f_open.writelines("AccountNo1\n")
            f_open.writelines("AccountNo2\n")
            f_open.writelines("AccountNo3\n")
            f_open.close()

    elif env_value == 'qa':
        # Call endpoint for "qa"
        print('Calling endpoint for qa')
        with open(script01_logfile, 'w') as f_open:
            f_open.writelines("AccountNo1\n")
            f_open.writelines("AccountNo2\n")
            f_open.writelines("AccountNo3\n")
            f_open.close()

    elif env_value == 'prod':
        # Call endpoint for "prod"
        print('Calling endpoint for prod')
        with open(script01_logfile, 'w') as f_open:
            f_open.writelines("AccountNo1\n")
            f_open.writelines("AccountNo2\n")
            f_open.writelines("AccountNo3\n")
            f_open.close()
    else:
        pass



if __name__ == "__main__":
   main(sys.argv[1:])



# Add and give file ownership to jenkins
# chown -R jenkins script01_log.txt


# Creating env variables
# export ENV_VALUE="dev"
# export ENV_VALUE="qa"
# export ENV_VALUE="prod"

# python3 script01.py -e $ENV_VALUE



# Callable syntaxes:
# python script01.py -e "dev"
# python script01.py -e "qa"
# python script01.py -e "prod"

# python script01.py --envValue="dev"
# python script01.py --envValue "dev"























