#!/usr/bin/bash
# script name: automate_health_check.sh
# Login to one account

# Using it to set variables
echo "Running script 01.."

# ACCESS_KEY_ID=$(cat aws_access01 | jq -r '.data.access_key')
# SECRET_ACCESS_KEY=$(cat aws_access01 | jq -r '.data.secret_key')
# SECURITY_TOKEN=$(cat aws_access01 | jq -r '.data.security_token')
ACCESS_KEY_ID=$(cat ${workspace}/app/aws_access01 | jq -r '.data.access_key')
SECRET_ACCESS_KEY=$(cat ${workspace}/app/aws_access01 | jq -r '.data.secret_key')
SECURITY_TOKEN=$(cat ${workspace}/app/aws_access01 | jq -r '.data.security_token')

AWS_REGION="us-east-2"
AWS_OUTPUT_FORMAT="json"

echo $ACCESS_KEY_ID
echo $SECRET_ACCESS_KEY
echo $SECURITY_TOKEN
echo $AWS_REGION
echo $AWS_OUTPUT_FORMAT

aws configure set aws_access_key_id "${ACCESS_KEY_ID}"
aws configure set aws_secret_access_key "${SECRET_ACCESS_KEY}"
aws configure set aws_session_token "${SECURITY_TOKEN}"
aws configure set region "${AWS_REGION}"
aws configure set output "${AWS_OUTPUT_FORMAT}"



# Real request
# whoami
# export ENV_VALUE="dev"

# Export our environment variable for account
# echo AWS_ACCESS_DETAILS=$ACCESS_KEY_ID > extravars.properties

echo "Checking environment variable!"
echo "Env Value is ${ENV_VALUE}"
# Run your aws cli operation here
# e.g aws s3 ls
python3 script01.py -e ${ENV_VALUE}
# python3 ${workspace}/app/script01.py -e ${ENV_VALUE}

echo "Operation Success!!"










