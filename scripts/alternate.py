import sys, os, time, getopt

# Accessing files in directories
if getattr(sys, 'frozen', False):
    # running in a bundled form
    base_dir = sys._MEIPASS # pylint: disable=no-member
else:
    # running normally
    base_dir = os.path.dirname(os.path.abspath(__file__))


# Locating helper files in the current working directory
config_file_path = os.path.join(base_dir, 'config.ini')





# # sys.argv is the list of command-line arguments.
# # len(sys.argv) is the number of command-line arguments.
# # Here sys.argv[0] is the program ie. script name.


# print('Number of arguments:', len(sys.argv), 'arguments.')
# print('Argument List:', str(sys.argv))




def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print('test.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt in ("-h", "--help"):
         print(f'USAGE: {sys.argv[0]} -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
   print('Input file is "', inputfile)
   print('Output file is "', outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])


# Callable syntaxes:
# python script01.py -i "input_file.txt" -o "output_file.txt"
# python script01.py --ifile="input_file.txt" --ofile="output_file.txt"
# python script01.py --ifile "input_file.txt" --ofile "output_file.txt"























